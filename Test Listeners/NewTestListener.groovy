import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class NewTestListener {
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def BeforeTestCase(TestCaseContext testCaseContext) {
		String testCaseId = testCaseContext.getTestCaseId()
		
		if (testCaseId.contains("Website")) {
			WebUI.openBrowser('')
		} else if (testCaseId.contains("Mobile")) {
			
			if (testCaseId.contains("Access profile") || testCaseId.contains("ChangeProfile")) {
				
				String rootDir = RunConfiguration.getProjectDir()
				
				Mobile.startApplication(rootDir + '/androidapp/DemoAppV2.apk', true)
		
				Mobile.tap(findTestObject('Mobile/HomePage/btnLoginHere'), 0)
		
				Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpEmail'), 'team1batch10@gmail.com')
		
				Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpPassword'), '@Password1')
		
				Mobile.tap(findTestObject('Mobile/LoginPage/btnLogin'), 0)
		
				Mobile.verifyElementExist(findTestObject('Mobile/HomePage/iconCart'), 0)
				
				Mobile.delay(2)
			} else if (testCaseId.contains("Login")) {
				
				String rootDir = RunConfiguration.getProjectDir()
				
				Mobile.startApplication(rootDir + '/androidapp/DemoAppV2.apk', true)
				
				Mobile.delay(2)
			}
		}
	}
	
	@AfterTestCase
	def AfterTestCase(TestCaseContext testCaseContext) {
		
		String testCaseId = testCaseContext.getTestCaseId()
		
		if (testCaseId.contains("Website")) {
			WebUI.openBrowser('')
		} else if (testCaseId.contains("Mobile")) {
			Mobile.closeApplication()
		}
	}	
}